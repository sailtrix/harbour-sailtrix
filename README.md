# Welcome to the Sailtrix GitLab

Sailtrix is a matrix client for SailfishOS supporting end-to-end encryption among many other features. Currently in beta state and is intended to become more feature-rich over time. This GitLab is used for code storage, issue management, and contributions. 

There are currently many features that are not implemented. Please see the <a href="https://gitlab.com/HengYeDev/harbour-sailtrix/-/issues">GitLab issue tracker</a> for an incomplete list. Feel free to add to it if you want a feature to be implemented.

## Donate

[PayPal](https://www.paypal.com/donate?business=YXF5TAF4A3H96&no_recurring=0&item_name=Assist+with+the+development+of+Sailtrix&currency_code=USD) or [Liberapay](https://liberapay.com/HengYe/donate)

# Legal notice

The software may be subject to the U.S. export control laws and regulations and by downloading the software the user certifies that he/she/it is authorized to do so in accordance with those export control laws and regulations.
