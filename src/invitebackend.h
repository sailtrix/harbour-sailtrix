#ifndef INVITEBACKEND_H
#define INVITEBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>
#include <Sailfish/Secrets/secretmanager.h>

class InviteBackend : public QObject
{
    Q_OBJECT
public:
    explicit InviteBackend(QObject *parent = nullptr);
    Q_INVOKABLE void join(QString room_id);
    Q_INVOKABLE void join_direct(QString room_id, QString uid);
    Q_INVOKABLE void reject(QString room_id);

signals:
    void joinDone();
    void rejectDone();
    void joinError();
private:
    QNetworkAccessManager* joiner;
    QNetworkAccessManager* rejector;
    QNetworkAccessManager* account_data_sender;
    Sailfish::Secrets::SecretManager m_secretManager;
    QString m_user_to_dm;
    QString m_user_id;
    QString m_access_token;
    QString m_hs_url;
private slots:
    void joined(QNetworkReply* reply);
    void rejected(QNetworkReply* reply);
    void accountDataSent(QNetworkReply* reply);
};

#endif // INVITEBACKEND_H
