#include "settingsbackend.h"
#include <QDir>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include "enc-util.h"

bool SettingsBackend::notificationDisabled() {
    return m_notification_disabled;
}

bool SettingsBackend::backgroundServiceDisabled() {
    return m_background_service_disabled;
}
int SettingsBackend::notifInterval() {
    return m_notif_interval;
}

int SettingsBackend::sortType() {
    return m_sort_type;
}

bool SettingsBackend::avatarsDisabled() {
    return m_avatars_disabled;
}

void SettingsBackend::save() {
    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        QJsonObject obj;
        obj.insert("notification_interval", notifInterval());
        obj.insert("notifications_disabled", notificationDisabled());
        obj.insert("sort_type", sortType());
        obj.insert("background_services_disabled", backgroundServiceDisabled());
        obj.insert("avatars_disabled", avatarsDisabled());

        doc.setObject(obj);
        settings.write(doc.toJson());
    }
}

void SettingsBackend::setNotificationDisabled(bool n) {
    m_notification_disabled = n;
    emit notificationDisabledChanged();
    save();
}

void SettingsBackend::setBackgroundServiceDisabled(bool n) {
    m_background_service_disabled = n;
    emit backgroundServiceDisabledChanged();
    save();
}

void SettingsBackend::setNotifInterval(int n) {
    m_notif_interval = n;
    emit notifIntervalChanged();
    save();
}

void SettingsBackend::setSortType(int n) {
    m_sort_type = n;
    emit sortTypeChanged();
    save();
}

void SettingsBackend::setAvatarsDisabled(bool n) {
    m_avatars_disabled = n;
    emit avatarsDisabledChanged();
    save();
}

SettingsBackend::SettingsBackend() : m_notification_disabled { false } {
    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(settings.readAll()).object();
        m_notification_disabled = obj.value("notifications_disabled").toBool();
        m_notif_interval = obj.value("notification_interval").toInt();
        m_sort_type = obj.value("sort_type").toInt();
        m_background_service_disabled = obj.value("background_services_disabled").toBool();
        m_avatars_disabled = obj.value("avatars_disabled").toBool();
        emit notificationDisabledChanged();
        emit notifIntervalChanged();
        emit sortTypeChanged();
        emit backgroundServiceDisabledChanged();
    }
}

void SettingsBackend::clear_cache() {
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();
    emit done();
}

void SettingsBackend::clear_config() {
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    connect(manager, &QNetworkAccessManager::finished, this, &SettingsBackend::after_logout);

    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        QString access_token = document.object().value("access_token").toString();
        QString hs_url = document.object().value("home_server").toString();

        QNetworkRequest req(hs_url + "/_matrix/client/r0/logout");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        manager->post(req, "");
    }

    delete_collection(&m_secretManager, QLatin1String("SailtrixWallet2"));

    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();


    QDir config(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();
}

void SettingsBackend::after_logout(QNetworkReply* reply) {
    qDebug() << "Here";
    emit configClearDone();
}
